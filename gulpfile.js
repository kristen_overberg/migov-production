'use strict';

var gulp 		= require('gulp'),
sass 		= require('gulp-sass'),
del			= require('del'),
lr 			= require('tiny-lr')(),
express 	= require('express'),
svgSprite 	= require('gulp-svg-sprite'),
svg2png 	= require('gulp-svg2png'),
prefix 		= require('gulp-autoprefixer'),
minifyCSS  	= require('gulp-minify-css'),
cssGlobbing = require('gulp-css-globbing'),
plumber = require('gulp-plumber'),
app 		= express();

var loadTasks = require('gulp-load')(gulp);


// Setup Express and Livereload server

var EXPRESS_PORT = 4000;
var EXPRESS_ROOT = __dirname;
var LIVERELOAD_PORT = 35729;
var watching = false;


function notifyLivereload(event) {
  var fileName = require('path').relative(EXPRESS_ROOT, event.path); 
  lr.changed({
    body: {
      files: [fileName]
    }
  });
}

//Configuring Sprites
var spriteConfig = {
  commonName: 'svg-common',
  mode: {
   css: {        
     sprite: "images/sprites/sprite.svg",
     dest: "",
     bust: false,
     render: {
       scss: {
         dest: "sass/base/_sprites.scss",
         template: "images-source/sprites/sprite.scss"
       }
     }
   }
 }
}

gulp.task('connect', function () {
  app.use(require('connect-livereload')());
  app.use(express.static(EXPRESS_ROOT));
  app.listen(EXPRESS_PORT);
  console.log('\n Serving migov at localhost:' + EXPRESS_PORT + '\n');

});

gulp.task('startLivereload', function() {
  lr.listen(LIVERELOAD_PORT);
  console.log('\n Started livereload server on port ' + LIVERELOAD_PORT + '\n');
});


// CSS Tasks
gulp.task('sass', function() {
	gulp.src('./sass/*.scss')
  .pipe(plumber())
  .pipe(cssGlobbing({
    extensions: '.scss'
  }))
  .pipe(sass())
  .pipe(prefix(["last 2 versions", "ie 8"], {cascade: true}))
  .pipe(minifyCSS())
  .pipe(gulp.dest('./css'));
});

//Image/Sprite Tasks
gulp.task('sprites', function() {
 gulp.src('images-source/sprites/*.svg')
 .pipe(plumber())
 .pipe(svgSprite(spriteConfig))
 .pipe(gulp.dest(''));
});

gulp.task('svg2png', function() {
  gulp.src('images/sprites/*.svg')
  .pipe(plumber())
  .pipe(svg2png())
  .pipe(gulp.dest('./images/sprites'));
});


// Watch
gulp.task('watch', function() {
	gulp.watch('./sass/**/**/*.scss', ['sass']);
  gulp.watch('images-source/sprites/*.svg', ['sprites', 'svg2png']);
  gulp.watch(['./css/*.css', '*.html', './js/*.js', './images/**'], notifyLivereload);
});

// Tasks
gulp.task('serve', ['connect','startLivereload']);
gulp.task('default', ['connect', 'startLivereload','watch']);


